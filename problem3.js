
function problem3(inventory){
    if(inventory){
        car_models=[]
        for(let i=0;i<inventory.length;i++){
            //  console.log(inventory[i]);
            car_models.push(inventory[i]['car_model'])
        }
        
        return (car_models.sort());
    }
    else{
        return ([]);
    }
}
module.exports=problem3;