function problem4(inventory){
    if(inventory){
        year_array=[]
        for(let i=0;i<inventory.length;i++){
            //  console.log(inventory[i]);
            stuff=[]
            stuff.push(inventory[i]['id'])
            stuff.push(inventory[i]['car_make'])
            stuff.push(inventory[i]['car_model'])
            stuff.push(inventory[i]['car_year'])
            year_array.push(stuff);
        }
        return(year_array);
    }
    else{
        return([]);
    }
}
module.exports=problem4;
